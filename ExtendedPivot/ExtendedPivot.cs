﻿using System.Windows;
using Microsoft.Phone.Controls;

namespace ExtendedPivot
{
    public class ExtendedPivot : Pivot
    {
        public static readonly DependencyProperty HeaderVisibilityProperty =
            DependencyProperty.Register("HeaderVisibilityProperty", typeof(Visibility), typeof(ExtendedPivot),
                new PropertyMetadata(null));

        public ExtendedPivot()
        {
            DefaultStyleKey = typeof (ExtendedPivot); // Set style of the control to the style defined in generic.xaml
        }

        public Visibility HeaderVisibility // Property to show/hide header of pivot
        {
            get { return (Visibility) GetValue(HeaderVisibilityProperty); }
            set { SetValue(HeaderVisibilityProperty, value); }
        }
    }
}